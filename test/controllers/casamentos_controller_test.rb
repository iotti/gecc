require 'test_helper'

class CasamentosControllerTest < ActionController::TestCase
  setup do
    @casamento = casamentos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:casamentos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create casamento" do
    assert_difference('Casamento.count') do
      post :create, casamento: { civil: @casamento.civil, data: @casamento.data, esposa: @casamento.esposa, esposo: @casamento.esposo, paroquia_id: @casamento.paroquia_id, religioso: @casamento.religioso, separados: @casamento.separados }
    end

    assert_redirected_to casamento_path(assigns(:casamento))
  end

  test "should show casamento" do
    get :show, id: @casamento
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @casamento
    assert_response :success
  end

  test "should update casamento" do
    patch :update, id: @casamento, casamento: { civil: @casamento.civil, data: @casamento.data, esposa: @casamento.esposa, esposo: @casamento.esposo, paroquia_id: @casamento.paroquia_id, religioso: @casamento.religioso, separados: @casamento.separados }
    assert_redirected_to casamento_path(assigns(:casamento))
  end

  test "should destroy casamento" do
    assert_difference('Casamento.count', -1) do
      delete :destroy, id: @casamento
    end

    assert_redirected_to casamentos_path
  end
end
