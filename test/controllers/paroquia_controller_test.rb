require 'test_helper'

class ParoquiaControllerTest < ActionController::TestCase
  setup do
    @paroquia = paroquia(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:paroquia)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create paroquia" do
    assert_difference('paroquia.count') do
      post :create, paroquia: { bairro: @paroquia.bairro, cidade: @paroquia.cidade, complemento: @paroquia.complemento, endereco: @paroquia.endereco, nome: @paroquia.nome, numero: @paroquia.numero, uf: @paroquia.uf }
    end

    assert_redirected_to paroquia_path(assigns(:paroquia))
  end

  test "should show paroquia" do
    get :show, id: @paroquia
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @paroquia
    assert_response :success
  end

  test "should update paroquia" do
    patch :update, id: @paroquia, paroquia: { bairro: @paroquia.bairro, cidade: @paroquia.cidade, complemento: @paroquia.complemento, endereco: @paroquia.endereco, nome: @paroquia.nome, numero: @paroquia.numero, uf: @paroquia.uf }
    assert_redirected_to paroquia_path(assigns(:paroquia))
  end

  test "should destroy paroquia" do
    assert_difference('paroquia.count', -1) do
      delete :destroy, id: @paroquia
    end

    assert_redirected_to paroquia_path
  end
end
