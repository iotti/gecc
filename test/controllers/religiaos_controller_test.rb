require 'test_helper'

class ReligiaosControllerTest < ActionController::TestCase
  setup do
    @religiao = religiaos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:religiaos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create religiao" do
    assert_difference('Religiao.count') do
      post :create, religiao: { nome: @religiao.nome }
    end

    assert_redirected_to religiao_path(assigns(:religiao))
  end

  test "should show religiao" do
    get :show, id: @religiao
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @religiao
    assert_response :success
  end

  test "should update religiao" do
    patch :update, id: @religiao, religiao: { nome: @religiao.nome }
    assert_redirected_to religiao_path(assigns(:religiao))
  end

  test "should destroy religiao" do
    assert_difference('Religiao.count', -1) do
      delete :destroy, id: @religiao
    end

    assert_redirected_to religiaos_path
  end
end
