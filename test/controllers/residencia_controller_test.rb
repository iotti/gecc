require 'test_helper'

class ResidenciaControllerTest < ActionController::TestCase
  setup do
    @residencium = residencia(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:residencia)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create residencium" do
    assert_difference('Residencium.count') do
      post :create, residencium: { bairro: @residencium.bairro, cidade: @residencium.cidade, complemento: @residencium.complemento, endereco: @residencium.endereco, esposa: @residencium.esposa, esposo: @residencium.esposo, numero: @residencium.numero, telefone: @residencium.telefone, uf: @residencium.uf }
    end

    assert_redirected_to residencium_path(assigns(:residencium))
  end

  test "should show residencium" do
    get :show, id: @residencium
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @residencium
    assert_response :success
  end

  test "should update residencium" do
    patch :update, id: @residencium, residencium: { bairro: @residencium.bairro, cidade: @residencium.cidade, complemento: @residencium.complemento, endereco: @residencium.endereco, esposa: @residencium.esposa, esposo: @residencium.esposo, numero: @residencium.numero, telefone: @residencium.telefone, uf: @residencium.uf }
    assert_redirected_to residencium_path(assigns(:residencium))
  end

  test "should destroy residencium" do
    assert_difference('Residencium.count', -1) do
      delete :destroy, id: @residencium
    end

    assert_redirected_to residencia_path
  end
end
