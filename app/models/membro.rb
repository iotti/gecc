class Membro < ActiveRecord::Base
    belongs_to :profissao
    belongs_to :paroquia
    belongs_to :usuario
    belongs_to :casamento
    belongs_to :religiao
    belongs_to :endereco
    belongs_to :endereco_profissional, class_name: 'Endereco', foreign_key: :endereco_profissao_id    
end
