class Usuario < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_one :esposo, class_name: 'Membro', foreign_key: :usuario_id
  has_one :esposa, class_name: 'Membro', foreign_key: :usuario_id

  

  accepts_nested_attributes_for :esposo
  accepts_nested_attributes_for :esposa
  belongs_to :perfil
end
