class ParoquiasController < GeccController
  before_action :set_paroquia, only: [:show, :edit, :update, :destroy]

  # GET /paroquia
  # GET /paroquia.json
  def index
    @paroquia = Paroquia.all
  end

  # GET /paroquia/1
  # GET /paroquia/1.json
  def show
  end

  # GET /paroquia/new
  def new
    @paroquia = Paroquia.new
  end

  # GET /paroquia/1/edit
  def edit
  end

  # POST /paroquia
  # POST /paroquia.json
  def create
    @paroquia = Paroquia.new(paroquia_params)

    respond_to do |format|
      if @paroquia.save
        format.html { redirect_to url_for(controller: :paroquias, action: :index), notice: 'paroquia was successfully created.' }
        format.json { render :show, status: :created, location: @paroquia }
      else
        format.html { render :new }
        format.json { render json: @paroquia.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /paroquia/1
  # PATCH/PUT /paroquia/1.json
  def update
    respond_to do |format|
      if @paroquia.update(paroquia_params)
        format.html { redirect_to @paroquia, notice: 'paroquia was successfully updated.' }
        format.json { render :show, status: :ok, location: @paroquia }
      else
        format.html { render :edit }
        format.json { render json: @paroquia.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /paroquia/1
  # DELETE /paroquia/1.json
  def destroy
    @paroquia.destroy
    respond_to do |format|
      format.html { redirect_to paroquia_url, notice: 'paroquia was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_paroquia
      @paroquia = Paroquia.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def paroquia_params
      params.require(:paroquia).permit(:nome, :endereco, :numero, :complemento, :bairro, :cidade, :uf)
    end
end
