class CasamentosController < GeccController
  before_action :set_casamento, only: [:show, :edit, :update, :destroy]

  # GET /casamentos
  # GET /casamentos.json
  def index
    @casamentos = Casamento.all
  end

  # GET /casamentos/1
  # GET /casamentos/1.json
  def show
  end

  # GET /casamentos/new
  def new
    @casamento = Casamento.new
  end

  # GET /casamentos/1/edit
  def edit
  end

  # POST /casamentos
  # POST /casamentos.json
  def create
    @casamento = Casamento.new(casamento_params)

    respond_to do |format|
      if @casamento.save
        format.html { redirect_to @casamento, notice: 'Casamento was successfully created.' }
        format.json { render :show, status: :created, location: @casamento }
      else
        format.html { render :new }
        format.json { render json: @casamento.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /casamentos/1
  # PATCH/PUT /casamentos/1.json
  def update
    respond_to do |format|
      if @casamento.update(casamento_params)
        format.html { redirect_to @casamento, notice: 'Casamento was successfully updated.' }
        format.json { render :show, status: :ok, location: @casamento }
      else
        format.html { render :edit }
        format.json { render json: @casamento.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /casamentos/1
  # DELETE /casamentos/1.json
  def destroy
    @casamento.destroy
    respond_to do |format|
      format.html { redirect_to casamentos_url, notice: 'Casamento was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_casamento
      @casamento = Casamento.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def casamento_params
      params.require(:casamento).permit(:paroquia_id, :esposo, :esposa, :data, :religioso, :civil, :separados)
    end
end
