class ReligiaosController < GeccController
  before_action :set_religiao, only: [:show, :edit, :update, :destroy]

  # GET /religiaos
  # GET /religiaos.json
  def index
    @religiaos = Religiao.all
  end

  # GET /religiaos/1
  # GET /religiaos/1.json
  def show
  end

  # GET /religiaos/new
  def new
    @religiao = Religiao.new
  end

  # GET /religiaos/1/edit
  def edit
  end

  # POST /religiaos
  # POST /religiaos.json
  def create
    @religiao = Religiao.new(religiao_params)

    respond_to do |format|
      if @religiao.save
        format.html { redirect_to @religiao, notice: 'Religiao was successfully created.' }
        format.json { render :show, status: :created, location: @religiao }
      else
        format.html { render :new }
        format.json { render json: @religiao.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /religiaos/1
  # PATCH/PUT /religiaos/1.json
  def update
    respond_to do |format|
      if @religiao.update(religiao_params)
        format.html { redirect_to @religiao, notice: 'Religiao was successfully updated.' }
        format.json { render :show, status: :ok, location: @religiao }
      else
        format.html { render :edit }
        format.json { render json: @religiao.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /religiaos/1
  # DELETE /religiaos/1.json
  def destroy
    @religiao.destroy
    respond_to do |format|
      format.html { redirect_to religiaos_url, notice: 'Religiao was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_religiao
      @religiao = Religiao.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def religiao_params
      params.require(:religiao).permit(:nome)
    end
end
