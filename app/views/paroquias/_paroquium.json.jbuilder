json.extract! paroquia, :id, :nome, :endereco, :numero, :complemento, :bairro, :cidade, :uf, :created_at, :updated_at
json.url paroquia_url(paroquia, format: :json)