json.extract! endereco, :id, :endereco, :numero, :complemento, :bairro, :cidade, :uf, :telefone, :created_at, :updated_at
json.url endereco_url(endereco, format: :json)