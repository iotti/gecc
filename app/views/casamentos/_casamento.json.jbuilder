json.extract! casamento, :id, :paroquia_id, :esposo, :esposa, :data, :religioso, :civil, :separados, :created_at, :updated_at
json.url casamento_url(casamento, format: :json)