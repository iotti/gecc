# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161216184357) do

  create_table "casamentos", force: :cascade do |t|
    t.integer  "paroquia_id", limit: 4
    t.integer  "esposo",      limit: 4
    t.integer  "esposa",      limit: 4
    t.date     "data"
    t.boolean  "religioso"
    t.boolean  "civil"
    t.boolean  "separados"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "casamentos", ["esposa"], name: "fk_rails_df7ae82a7c", using: :btree
  add_index "casamentos", ["esposo"], name: "fk_rails_06a361a2c5", using: :btree
  add_index "casamentos", ["paroquia_id"], name: "fk_rails_05e10cfb66", using: :btree

  create_table "enderecos", force: :cascade do |t|
    t.string   "endereco",    limit: 255
    t.integer  "numero",      limit: 4
    t.string   "complemento", limit: 255
    t.string   "bairro",      limit: 255
    t.string   "cidade",      limit: 255
    t.string   "uf",          limit: 255
    t.string   "telefone",    limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "filhos", force: :cascade do |t|
    t.integer  "casamento_id",    limit: 4
    t.string   "nome",            limit: 255
    t.date     "data_nascimento"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "filhos", ["casamento_id"], name: "fk_rails_ab5f3bc0fa", using: :btree

  create_table "membros", force: :cascade do |t|
    t.integer  "paroquia_id",              limit: 4
    t.string   "sexo",                     limit: 255
    t.string   "nome",                     limit: 255
    t.string   "apelido",                  limit: 255
    t.date     "data_nascimento"
    t.integer  "profissao_id",             limit: 4
    t.integer  "casamento_id",             limit: 4
    t.integer  "usuario_id",               limit: 4
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.integer  "endereco_id",              limit: 4
    t.integer  "endereco_profissional_id", limit: 4
    t.integer  "religiao_id",              limit: 4
  end

  add_index "membros", ["casamento_id"], name: "fk_rails_ada275c097", using: :btree
  add_index "membros", ["endereco_id"], name: "fk_rails_4fd1725bd5", using: :btree
  add_index "membros", ["endereco_profissional_id"], name: "fk_rails_bd30cad86c", using: :btree
  add_index "membros", ["paroquia_id"], name: "fk_rails_cfb34ed26c", using: :btree
  add_index "membros", ["profissao_id"], name: "fk_rails_ac4041b7aa", using: :btree
  add_index "membros", ["religiao_id"], name: "fk_rails_fae478998b", using: :btree
  add_index "membros", ["usuario_id"], name: "fk_rails_86237f751b", using: :btree

  create_table "paroquias", force: :cascade do |t|
    t.string   "nome",        limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "endereco_id", limit: 4
  end

  add_index "paroquias", ["endereco_id"], name: "fk_rails_52c05fae11", using: :btree

  create_table "perfils", force: :cascade do |t|
    t.string   "nome",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "profissaos", force: :cascade do |t|
    t.string   "descricao",  limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "religiaos", force: :cascade do |t|
    t.string   "nome",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "usuarios", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.integer  "perfil_id",              limit: 4,   default: 2
  end

  add_index "usuarios", ["email"], name: "index_usuarios_on_email", unique: true, using: :btree
  add_index "usuarios", ["perfil_id"], name: "fk_rails_f39df9db65", using: :btree
  add_index "usuarios", ["reset_password_token"], name: "index_usuarios_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "casamentos", "membros", column: "esposa"
  add_foreign_key "casamentos", "membros", column: "esposo"
  add_foreign_key "casamentos", "paroquias"
  add_foreign_key "filhos", "casamentos"
  add_foreign_key "membros", "casamentos"
  add_foreign_key "membros", "enderecos"
  add_foreign_key "membros", "enderecos", column: "endereco_profissional_id"
  add_foreign_key "membros", "paroquias"
  add_foreign_key "membros", "profissaos"
  add_foreign_key "membros", "religiaos"
  add_foreign_key "membros", "usuarios"
  add_foreign_key "paroquias", "enderecos"
  add_foreign_key "usuarios", "perfils"
end
