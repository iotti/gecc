class CreateParoquia < ActiveRecord::Migration
  def change
    create_table :paroquias do |t|
      t.string :nome
      t.string :endereco
      t.integer :numero
      t.string :complemento
      t.string :bairro
      t.string :cidade
      t.string :uf

      t.timestamps null: false
    end
  end
end
