class CreateMembros < ActiveRecord::Migration
  def change
    create_table :membros do |t|
      t.integer :paroquia_id
      t.string :sexo
      t.string :nome
      t.string :apelido
      t.date :data_nascimento
      t.integer :profissao_id
      t.string :endereco_profissao
      t.string :telefone_profissao
      t.string :religiao
      t.integer :casamento_id
      t.integer :usuario_id

      t.timestamps null: false
    end
  end
end
