class AddKeys < ActiveRecord::Migration
  def change
    
    add_foreign_key :membros, :usuarios
    add_foreign_key :membros, :paroquias
    add_foreign_key :membros, :profissaos
    add_foreign_key :membros, :casamentos

    add_foreign_key :residencias, :usuarios
    add_foreign_key :residencias, :membros, column: :esposo
    add_foreign_key :residencias, :membros, column: :esposa

    add_foreign_key :casamentos, :membros, column: :esposo
    add_foreign_key :casamentos, :membros, column: :esposa
    add_foreign_key :casamentos, :paroquias
    
    add_foreign_key :filhos,:casamentos
  end
end
