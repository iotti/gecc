class CreateReligiaos < ActiveRecord::Migration
  def change
    create_table :religiaos do |t|
      t.string :nome

      t.timestamps null: false
    end

    remove_column :membros, :religiao
    add_column :membros, :religiao_id, :integer
    add_foreign_key :membros, :religiaos
  end
end
