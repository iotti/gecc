class CreatePerfils < ActiveRecord::Migration
  def change
    create_table :perfils do |t|
      t.string :nome

      t.timestamps null: false
    end
    Perfil.create(nome:'Administrador')
    p = Perfil.create(nome:'Usuario')
    
    add_column :usuarios, :perfil_id, :integer, default:p.id

    add_foreign_key :usuarios, :perfils
  end
end
