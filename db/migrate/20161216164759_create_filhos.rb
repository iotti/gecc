class CreateFilhos < ActiveRecord::Migration
  def change
    create_table :filhos do |t|
      t.integer :casamento_id
      t.string :nome
      t.date :data_nascimento

      t.timestamps null: false
    end
  end
end
