class CreateResidencia < ActiveRecord::Migration
  def change
    create_table :residencias do |t|
      t.integer :esposo
      t.integer :esposa
      t.string :endereco
      t.integer :numero
      t.string :complemento
      t.string :bairro
      t.string :cidade
      t.string :uf
      t.string :telefone
      t.integer :usuario_id

      t.timestamps null: false
    end
  end
end
