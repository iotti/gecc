class CreateCasamentos < ActiveRecord::Migration
  def change
    create_table :casamentos do |t|
      t.integer :paroquia_id
      t.integer :esposo
      t.integer :esposa
      t.date :data
      t.boolean :religioso
      t.boolean :civil
      t.boolean :separados

      t.timestamps null: false
    end
  end
end
