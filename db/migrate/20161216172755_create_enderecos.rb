class CreateEnderecos < ActiveRecord::Migration
  def change
    remove_column :membros, :endereco_profissao
    remove_column :membros, :telefone_profissao
    remove_column :paroquias, :endereco
    remove_column :paroquias, :numero
    remove_column :paroquias, :complemento
    remove_column :paroquias, :bairro
    remove_column :paroquias, :cidade
    remove_column :paroquias, :uf

    
    create_table :enderecos do |t|
      t.string :endereco
      t.integer :numero
      t.string :complemento
      t.string :bairro
      t.string :cidade
      t.string :uf
      t.string :telefone

      t.timestamps null: false
    end

    add_column :membros, :endereco_id, :integer
    add_column :membros, :endereco_profissional_id, :integer
    add_column :paroquias, :endereco_id, :integer

    add_foreign_key :membros, :enderecos
    add_foreign_key :membros, :enderecos, column: :endereco_profissional_id
    add_foreign_key :paroquias, :enderecos
  end
end
